import time
import requests
import json
import const
from pprint import pprint


def answer_user_bot(data):
    data = {
        'chat_id': const.MY_ID,
        'text': data
    }
    url = const.URL.format(token=const.TOKEN, method=const.SEND_METH)
    response = requests.post(url, data=data)


def parse_weather_data(data):
    for elem in data['weather']:
        weather_state = elem['main']
    temp = round(data['main']['temp'] - 273.15)
    city = data['name']
    msg = f'The weather in {city}:👇\n🌡Temp is {temp}\n🌦State is {weather_state}'
    return msg


def get_weather(location):
    url = const.WEATHER_URL.format(city=location, token=const.WEATHER_TOKEN)
    response = requests.get(url)
    if location == '/start':
        return 'Welcome to the chat bot!😊\n' \
               'Enter the name of the city ' \
               'in which you want to specify the weather.'
    if response.status_code != 200:
        return 'Sorry, this city not found😑'
    data = json.loads(response.content)
    return parse_weather_data(data)


def get_massage(data):
    return data['message']['text']


def save_update_id(update):
    pprint(update)
    with open(const.FILE_ID_FILE_PATH, 'w') as file:
        file.write(str(update['update_id']))
    const.UPDATE_ID = update['update_id']
    return True


def main():
    while True:
        url = const.URL.format(token=const.TOKEN, method=const.UPDATE_METH)
        content = requests.get(url).text
        data = json.loads(content)
        result = data['result'][::-1]
        needed_part = None
        for elem in result:
            if elem['message']['chat']['id'] == const.MY_ID:
                needed_part = elem
                break
        if const.UPDATE_ID != needed_part['update_id']:
            massage = get_massage(needed_part)
            msg = get_weather(massage)
            answer_user_bot(msg)
            save_update_id(needed_part)
        time.sleep(1)


if __name__ == '__main__':
    main()
